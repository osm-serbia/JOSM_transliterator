package org.openstreetmap.josm.plugins.serbiantransliterator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultListModel;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;

public class UpitSaServeraZaUlicuDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JList<String> list_1;
	private String result;

	static int pozicijaProzora_x = 100;
	static int pozicijaProzora_y = 100;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PovezivanjeUlicaSaRGZ povezivanje = new PovezivanjeUlicaSaRGZ("741892");
			UpitSaServeraZaUlicuDialog dialog = new UpitSaServeraZaUlicuDialog(povezivanje, "ЈОВАНА ДУЧИЋА", false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
			String result = dialog.showDialog();
			System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String showDialog() {
		    setVisible(true);
		    return result;
	}


	public UpitSaServeraZaUlicuDialog(PovezivanjeUlicaSaRGZ povezivanje, String nazivUlice, boolean daLiUzimaNaseljeUobzir) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setAlwaysOnTop(true);
		setBounds(pozicijaProzora_x, pozicijaProzora_y, 700, 400);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(600, 300));
		contentPanel.add(scrollPane);

		String rezultat = povezivanje.napraviUpitZaUlicu(nazivUlice, daLiUzimaNaseljeUobzir);
		String[] rez = rezultat.split("\n");

		DefaultListModel<String> listModel = new DefaultListModel<String>();

		for (String string : rez) {
			listModel.addElement(string);	
		}

		list_1 = new JList<String>(listModel);
		list_1.setVisibleRowCount(10);
		
		list_1.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        JList list = (JList)evt.getSource();
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		            System.out.println(" izabrano " + list.getSelectedValue());
		            result = (String) list.getSelectedValue();
					
					pozicijaProzora_x = getBounds().x;
					pozicijaProzora_y = getBounds().y;
					setVisible(false);
					dispose();
		        } 
		    }
		});

		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(list_1);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					dispose();
				}
			});
			
			JButton btnNewButton = new JButton("ОК");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
		            result = (String) list_1.getSelectedValue();
		            System.out.println("*****"+result);
					pozicijaProzora_x = getBounds().x;
					pozicijaProzora_y = getBounds().y;
					setVisible(false);
					dispose();
				}
			});
			buttonPane.add(btnNewButton);
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}
		
		// zatvara na ESC
		ActionListener escListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UpitSaServeraZaUlicuDialog.this.dispose();		        }
		};
		UpitSaServeraZaUlicuDialog.this.getRootPane().registerKeyboardAction(escListener,
				KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				JComponent.WHEN_IN_FOCUSED_WINDOW);
	}

}
