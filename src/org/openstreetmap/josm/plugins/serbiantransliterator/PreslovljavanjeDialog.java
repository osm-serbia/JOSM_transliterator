package org.openstreetmap.josm.plugins.serbiantransliterator;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;

import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.command.ChangePropertyCommand;
import org.openstreetmap.josm.command.Command;
import org.openstreetmap.josm.command.SequenceCommand;
import org.openstreetmap.josm.data.UndoRedoHandler;
import org.openstreetmap.josm.data.osm.OsmPrimitive;

enum EIdKoloneUTabeli{
	NAME(6), CIRILICA(4), LATINICA(5), ENGLESKI(7), HASH(3), RGZ_MB(7);
	private int kolona;

	private EIdKoloneUTabeli(int col) {
		this.setKolona(col);
	}

	public int getKolona() {
		return kolona;
	}

	public void setKolona(int kolona) {
		this.kolona = kolona;
	}
}

/**
 * @author mpele
 *
 */
public class PreslovljavanjeDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4717990196196081917L;
	private final JPanel contentPanel = new JPanel();
	private boolean mbExpert = false;
	private boolean mbLatinicaSeMenja;
	private JTable mTable;
	JComboBox<Object> mComboBox;

	PreslovljavanjeTableModel mModel;
	PodrazumevanoPismo mPodrazumevanoPismoAutoPreslovljavanje;
	private String mTagZaRGZUlicu;
	private PovezivanjeUlicaSaRGZ povezivanjeUlicaSaRGZ;
	private JButton btnRGZ;
	
	static int pozicijaProzora_x = 100;
	static int pozicijaProzora_y = 100;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PreslovljavanjeDialog dialog = new PreslovljavanjeDialog();

			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

			dialog.setVisible(true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Konstruktor bez dodatnih tagova
	 */
	public PreslovljavanjeDialog() {
		this(new ArrayList<String>(),PodrazumevanoPismo.BEZ_PROMENE, false, true, "", "rgzTag");
	}


	/**
	 * Glavni konstruktor
	 * Konstruktor sa dodatnim tagovima i da li je ekspert (da li radi sa relacijama)
	 * @param string 
	 */		
	public PreslovljavanjeDialog(ArrayList<String> dodatniTagovi, PodrazumevanoPismo podrazumevanoPismo, 
			boolean bExpert, boolean bLatinica, String maticniBrojNaselja, String tagZaRGZUlicu) {
		setModal(true);
		setTitle("Пресловљавање");
		setBounds(pozicijaProzora_x, pozicijaProzora_y, 1300, 700);
		// setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		povezivanjeUlicaSaRGZ = new PovezivanjeUlicaSaRGZ(maticniBrojNaselja);
		mTagZaRGZUlicu = tagZaRGZUlicu;
		{
			// mModel = new DefaultTableModel();
			mModel = new PreslovljavanjeTableModel();
			dodatniTagovi.add(0, mTagZaRGZUlicu);
			mModel.setDodatniTagoviList(dodatniTagovi);
			mModel.popuniModelSaKolonama();
			mModel.setPovezivanjeUlicaSaRGZ(povezivanjeUlicaSaRGZ);
			mTable = new JTable(mModel);
			mTable.setAutoCreateRowSorter(true);

			mbExpert = bExpert;
			mbLatinicaSeMenja = bLatinica;

			ucitajSveTagoveIzSelektovanihObjekata(podrazumevanoPismo);
			mModel.setPodrazumevanoPismo(podrazumevanoPismo);

			// Sirina kolona
			TableColumn column = mTable.getColumnModel().getColumn(0);
			column.setMinWidth(100);
			column.setMaxWidth(100);
			column = mTable.getColumnModel().getColumn(1);
			column.setMinWidth(50);
			column.setMaxWidth(50);
			column = mTable.getColumnModel().getColumn(2);
			column.setMinWidth(50);
			column.setMaxWidth(50);
			column = mTable.getColumnModel().getColumn(EIdKoloneUTabeli.HASH.getKolona());
			column.setMinWidth(50);
			column.setMaxWidth(50);

			// definise render
			mTable.setDefaultRenderer(String.class, new StringRenderer());
			mTable.setDefaultRenderer(Boolean.class, new CheckBoxRenderer());
			mTable.setDefaultRenderer(PreslovljavanjeOSM.class, new IdRender());

			// definise precice sa tastature
			InputMap inputMap = mTable.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
			ActionMap actionMap = mTable.getActionMap();

			// pritisnuto Ctrl-S - preslovi iz sr
			inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK), "ControlS");
			actionMap.put("ControlS", new AbstractAction() {
				private static final long serialVersionUID = 1L;
				public void actionPerformed(ActionEvent e) {
					presloviSelektovanoIzSr();
				}
			});

			// pritisnuto Ctrl-D - Presovi auto
			inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK), "ControlD");
			actionMap.put("ControlD", new AbstractAction() {
				private static final long serialVersionUID = 1L;
				public void actionPerformed(ActionEvent e) {
					presloviSelektovanoAuto();
				}
			});

			// pritisnuto Ctrl-F - original
			inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK), "ControlF");
			actionMap.put("ControlF", new AbstractAction() {
				private static final long serialVersionUID = 1L;
				public void actionPerformed(ActionEvent e) {
					vratiSelektovaneNaOriginale();
				}
			});



			// dupli klik
			mTable.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e){
					if (e.getClickCount() == 2){
						try {
							JTable target = (JTable)e.getSource();
							int row = target.getSelectedRow();
							int col = target.getSelectedColumn();
							PreslovljavanjeOSM tmpPres = (PreslovljavanjeOSM)target.getValueAt(row, 0) ;
							if(col==0)
								Desktop.getDesktop().browse(new URI("http://www.openstreetmap.org/browse/"+tmpPres.getTipObjekta()+"/"+tmpPres.getId()));
							else
								Desktop.getDesktop().browse(new URI("http://www.openstreetmap.org/?"+tmpPres.getTipObjekta()+"="+tmpPres.getId()));

						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (URISyntaxException e1) {
							e1.printStackTrace();
						}
					}
				}
			} );


			JScrollPane scroller = new JScrollPane(mTable);
			contentPanel.add(scroller, BorderLayout.CENTER);
		}
		{
			final JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				JLabel lblPodrazumevanoPismo = new JLabel("Подразумевано писмо");
				panel.add(lblPodrazumevanoPismo);
			}
			{
				mComboBox = new JComboBox<Object>();
				mComboBox.setModel(new DefaultComboBoxModel<Object>(PodrazumevanoPismo.values()));
				mComboBox.setSelectedIndex(1);
				mComboBox.setToolTipText("Za preslovljavanje iz sr");

				mComboBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mModel.setPodrazumevanoPismo((PodrazumevanoPismo) mComboBox.getSelectedItem());
					}
				});
				panel.add(mComboBox);
			}
			{
				JButton btnRemove = new JButton("Уклони редове");
				panel.add(btnRemove);
				btnRemove.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { // brise
						// oznacene
						// redove
						int[] rows = mTable.getSelectedRows();
						for (int i = 0; i < rows.length; i++) {
							mModel.removeRow(rows[i] - i);
						}
					}
				});
			}
			{
				JButton btnPreslovi = new JButton("Преслови из name:sr");
				panel.add(btnPreslovi);
				btnPreslovi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						presloviSelektovanoIzSr();
					}
				});
				btnPreslovi.setToolTipText("Ctrl+S");
			}
			{
				JButton btnPresloviAuto = new JButton("Преслови аутоматски");
				panel.add(btnPresloviAuto);
				btnPresloviAuto.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						presloviSelektovanoAuto();
					}
				});
				btnPresloviAuto.setToolTipText("Ctrl+D");
			}
			{
				// vraca originalne zapise
				JButton btnOriginal = new JButton("Оригинал");
				panel.add(btnOriginal);
				btnOriginal.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						vratiSelektovaneNaOriginale();
					}
				});
				btnOriginal.setToolTipText("Ctrl+F");
			}
			{
				// Uparuje sa RGZom
				btnRGZ = new JButton("РГЗ");
				panel.add(btnRGZ);
				btnRGZ.setToolTipText("Када је селектовано више редова онда упарује по тачном називу");
				btnRGZ.setEnabled(false);
				btnRGZ.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						int[] rows = mTable.getSelectedRows();
						if(rows.length == 1) { // kada je selektvan jedan red onda trazi upit slicnih
							UpitSaServeraZaUlicuDialog dialog = new UpitSaServeraZaUlicuDialog(povezivanjeUlicaSaRGZ, (String) mTable.getValueAt(rows[0], EIdKoloneUTabeli.NAME.getKolona()), true);
							dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

							String result = dialog.showDialog();
							if(result != null) {								
								mTable.setValueAt(result.split(" ")[0], rows[0], EIdKoloneUTabeli.RGZ_MB.getKolona());
								mTable.setValueAt(true, rows[0], 2); // checkbox za snimanje
							}
						}
						else {// kada je selektvano vise redova onda uparuje po tacnom nazivu
							if(!maticniBrojNaselja.equals("?")) {
								selektovaniUpariSaRGZom();
							}
							else {
								JOptionPane.showMessageDialog(null, "Ако није унет матични број насеља претрага ради само у појединачном режиму!","Обавештење!", JOptionPane.WARNING_MESSAGE);
							}
						}
					}
				});
			}
			{
				// Uparuje sa RGZom
				JButton btnRGZ2 = new JButton("РГЗ без насеља");
				panel.add(btnRGZ2);
				btnRGZ2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						int[] rows = mTable.getSelectedRows();
						UpitSaServeraZaUlicuDialog dialog = new UpitSaServeraZaUlicuDialog(povezivanjeUlicaSaRGZ, (String) mTable.getValueAt(rows[0], EIdKoloneUTabeli.NAME.getKolona()), false);
						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

						String result = dialog.showDialog();
						if(result != null) {								
							mTable.setValueAt(result.split(" ")[0], rows[0], EIdKoloneUTabeli.RGZ_MB.getKolona());
							mTable.setValueAt(true, rows[0], 2); // checkbox za snimanje
						}
					}
				});
			}
			{
				// Кoпира РГЗ матични број на остале селектоване ентитете
				JButton btnRGZ_propagacija = new JButton("Пропагација РГЗ броја");
				panel.add(btnRGZ_propagacija);
				btnRGZ_propagacija.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) { 
						int[] rows = mTable.getSelectedRows();
						
						if(rows.length < 2) {
							JOptionPane.showMessageDialog(null, "Moraju da budu selektovana bar dva reda.","Обавештење!", JOptionPane.WARNING_MESSAGE);
							return;
						}
						
						String mb = null;
						
						for (int i = 0; i < rows.length; i++) {
							// int row = mTable.convertRowIndexToModel(rows[i]);
							String tmp = (String) mTable.getValueAt(rows[i], EIdKoloneUTabeli.RGZ_MB.getKolona());

							if(tmp.length()>3) {
								mb = tmp;
							}
							System.out.println(tmp);
						}

						System.out.println("RGZ id za propagaciju: "+ mb);
	
						if(mb.length()<3) {
							JOptionPane.showMessageDialog(null, "Nije pronadjen broj koji bi se propagirao na sve selektovane elemente!","Обавештење!", JOptionPane.WARNING_MESSAGE);							
						}
						else {
							for (int i = 0; i < rows.length; i++) {
								int row = mTable.convertRowIndexToModel(rows[i]);
								mModel.propagiranjeRGZbroja(row, mb);
							}							
						}
						
					}
				});
				btnRGZ_propagacija.setToolTipText("Копира РГЗ матични број на остале селектоване ентитете");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						pozicijaProzora_x = getBounds().x;
						pozicijaProzora_y = getBounds().y;
						updateJOSMSelection();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						PreslovljavanjeDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}



	protected void selektovaniUpariSaRGZom() {
		int[] rows = mTable.getSelectedRows();
		for (int i = 0; i < rows.length; i++) {
			int row = mTable.convertRowIndexToModel(rows[i]);
			mModel.upariSaRGZom(row);
		}		
	}

	private void presloviSelektovanoIzSr(){
		int[] rows = mTable.getSelectedRows();
		for (int i = 0; i < rows.length; i++) {
			// da prebaci broj reda iz sortirane tabele u red po
			// modelu
			int row = mTable.convertRowIndexToModel(rows[i]);
			mModel.presloviRedIzSr(row, mbLatinicaSeMenja);
		}
	}

	private void presloviSelektovanoAuto(){
		int[] rows = mTable.getSelectedRows();
		for (int i = 0; i < rows.length; i++) {
			// da prebaci broj reda iz sortirane tabele u red po
			// modelu
			int row = mTable.convertRowIndexToModel(rows[i]);
			mModel.presloviRedAuto(row, mbLatinicaSeMenja);
		}
	}

	private void vratiSelektovaneNaOriginale(){
		// vraca originalne zapise
		int[] rows = mTable.getSelectedRows();
		for (int i = 0; i < rows.length; i++) {
			// da prebaci broj reda iz sortirane tabele u red po
			// modelu
			int row = mTable.convertRowIndexToModel(rows[i]);
			mModel.presloviRedOriginal(row);
		}		
	}

	public void ucitajSveTagoveIzSelektovanihObjekata(PodrazumevanoPismo podrazumevanoPismo) {
		// zastita ako se ne pokrece iz JOSM-a vec iz Eclipse-a
		try{
			MainApplication.getLayerManager().getEditDataSet().getSelected();
		}
		catch(Exception e){
			return;
		}


		Collection<OsmPrimitive> selection = MainApplication.getLayerManager().getEditDataSet().getSelected();

		boolean obavestenje = false;
		for (OsmPrimitive element : selection) {
			
			if(element.getId() ==0 && obavestenje == false) { // upozorenje na tek kreirane elemente
				JOptionPane.showMessageDialog(null, "Постоји елемент који је управо креиран и још нема додељен id!\n Потребно је прво урадити upload постојећих измена на сервер и потом презузети исте податке са сервера (Update).","Обавештење!", JOptionPane.WARNING_MESSAGE);
				obavestenje = true;
			}
			
			PreslovljavanjeOSM preslovljavanjeOSM = new PreslovljavanjeOSM();
			preslovljavanjeOSM.setPodrazumevanoPismo(podrazumevanoPismo);
			preslovljavanjeOSM.setTipObjekta(element.getType().toString());			
			preslovljavanjeOSM.setId(element.getId());
			for (String key : element.keySet()) {
				String value = element.get(key);
				preslovljavanjeOSM.setNameKeyValue(key, value);
			}

			//preslovljavanjeOSM.setNameKeyValue(mTagZaRGZUlicu, povezivanjeSaUlicaSaRGZ.nadjiMBulice(preslovljavanjeOSM.getName()));

			// ako ima tagova za naziv upisuje u tabelu
			if (preslovljavanjeOSM.daLiImaTagovaZaNaziv() ) {
				// ne upisuje relacije ako nije expert
				System.out.println(preslovljavanjeOSM.getTipObjekta());
				System.out.println(mbExpert);
				if( (mbExpert && preslovljavanjeOSM.getTipObjekta().equals("relation") ) ||
						! preslovljavanjeOSM.getTipObjekta().equals("relation")){
					//System.out.println("Upisuje");
					insert(preslovljavanjeOSM);
				}
			}			
		}
	}

	/**
	 * Ubacuje red u tabelu na osnovu svih tagova
	 * 
	 * @param preslovljavanjeOSM
	 */
	public void insert(PreslovljavanjeOSM preslovljavanjeOSM) {
		//		System.out.println(preslovljavanjeOSM.daLiImaIzmena()
		//				+ preslovljavanjeOSM.pregled());
		//		System.out.println("upis u tabelu" + preslovljavanjeOSM.getId() + " "
		//				+ preslovljavanjeOSM.getName() + " "
		//				+ preslovljavanjeOSM.getName_sr() + " "
		//				+ preslovljavanjeOSM.getName_sr_lat() + " "
		//				+ preslovljavanjeOSM.getName_en());
		mModel.addRed(preslovljavanjeOSM);
	}

	public void updateJOSMSelection() {
		int numRow;
		int brojKolona = mModel.getColumnCount();

		// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		Collection<Command> c = new LinkedList<Command>();
		Collection<OsmPrimitive> selection = MainApplication.getLayerManager().getEditDataSet().getSelected();
		for (OsmPrimitive element : selection) {
			numRow = getRowZapisa(element.getId()); // vraca -1 ako nije stiklirano da treba da se menja
			if (numRow >= 0) {

				// kreira JOSM komande za azuriranje tagova 
				c.add(new ChangePropertyCommand(element, Tagovi.NAME.getTagKey(), 
						mModel.getValueAt(numRow, EIdKoloneUTabeli.NAME.getKolona()).toString()));
				c.add(new ChangePropertyCommand(element, Tagovi.CIRILICA.getTagKey(), 
						mModel.getValueAt(numRow, EIdKoloneUTabeli.CIRILICA.getKolona()).toString()));
				c.add(new ChangePropertyCommand(element, Tagovi.LATINICA.getTagKey(), mModel.getValueAt(numRow, EIdKoloneUTabeli.LATINICA.getKolona()).toString()));

				// prati hash sumu
				if((Boolean) mModel.getValueAt(numRow, EIdKoloneUTabeli.HASH.getKolona())){ // ako je oznacen chechkbox
					String pomString=mModel.getValueAt(numRow, EIdKoloneUTabeli.NAME.getKolona()).toString()
							+mModel.getValueAt(numRow, EIdKoloneUTabeli.CIRILICA.getKolona()).toString()
							+mModel.getValueAt(numRow, EIdKoloneUTabeli.LATINICA.getKolona()).toString();

					c.add(new ChangePropertyCommand(element, Tagovi.HASH.getTagKey(), 
							Integer.toString(PreslovljavanjeOSM.getHash(pomString))));
				} else { // ako nije stiklirano a postoji hash onda ga brise
					if(((PreslovljavanjeOSM) mModel.getValueAt(numRow, 0)).daLiImaHash()){
						c.add(new ChangePropertyCommand(element, Tagovi.HASH.getTagKey(), ""));
					}
				}

				// za kolone sa dodatnim tagovima
				for(int i = EIdKoloneUTabeli.NAME.getKolona()+1; i< brojKolona;i++){
					String key = mModel.getColumnName(i);
					String vrednostUTabeli = mModel.getValueAt(numRow, i).toString();
					c.add(new ChangePropertyCommand(element, key, vrednostUTabeli));	
				}
			}
		}

		if(!c.isEmpty()) {
			SequenceCommand command = new SequenceCommand(
					//trn("Updating properties of up to {0} object", "Updating properties of up to {0} objects", selection.size(), selection.size()),
					"Promena naziva",
					c
					);
			// executes the commands and adds them to the undo/redo chains
			UndoRedoHandler.getInstance().add(command);
		}
		else {
			JOptionPane.showMessageDialog(null, "Ни један елемент није означен да треба да се ажурира (да се сниме измене)!","Обавештење!", JOptionPane.WARNING_MESSAGE);
		}

		// //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		// zatvara dijalog
		PreslovljavanjeDialog.this.dispose();
	}

	/**
	 * Vraca id reda u kome se nalaze podaci za objekat ili -1 ako ga nema ili
	 * nije stikliran da snimi izmene
	 * 
	 * @param id
	 * @return
	 */
	private int getRowZapisa(long id) {
		for (int i = 0; i < mModel.getRowCount(); i++) {
			// System.out.println(id+" "+i+" "+mModel.getRowCount());
			if (id == Long.parseLong(mModel.getValueAt(i, 0).toString())) {
				// System.out.println("i - "+i+" "+mModel.getValueAt(i,
				// 3).toString());
				Boolean bIzmena = (Boolean) mModel.getValueAt(i, 2);

				// ako nije stiklirano da se menja ne menja
				if (bIzmena == true)
					return i;
				else
					return -1; // ne snima

			}
		}
		return -1;
	}

	public void setRGZ_naselje(String punNazivNaseljaString) {
		setTitle(getTitle() + " "+ punNazivNaseljaString);
		btnRGZ.setEnabled(true);
	}
}

