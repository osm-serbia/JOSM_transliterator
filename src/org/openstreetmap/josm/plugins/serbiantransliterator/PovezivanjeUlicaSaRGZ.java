package org.openstreetmap.josm.plugins.serbiantransliterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

public class PovezivanjeUlicaSaRGZ {

	protected Map<String, String> mRGZ_MB_RGZaMap = new HashMap<String, String>();
	private String mMaticniBrojNaselja;


	public static void main(String[] args) {
		//PovezivanjeUlicaSaRGZ povezivanje = new PovezivanjeUlicaSaRGZ("741892");
		PovezivanjeUlicaSaRGZ povezivanje = new PovezivanjeUlicaSaRGZ("?");
		//Map<String, String> mapa = povezivanje.getRGZ_MB_RGZaMap();
		//String mb = povezivanje.nadjiMBulicu("ЈОВАНА ДУЧИЋА");

		//System.out.println(mapa);
		//povezivanje.izbaciSaSpiskaMBUlice(mb);
		//System.out.println(mapa);

		povezivanje.napraviUpitZaUlicu("ЈОВАНА ДУЧИЋА", true);
	}


	public PovezivanjeUlicaSaRGZ(String pMaticniBrojNaselja) {

		String line;

		if(pMaticniBrojNaselja.length()>1)
			mMaticniBrojNaselja = pMaticniBrojNaselja;
		else {
			mMaticniBrojNaselja = "?";
		}

		String st = "https://www.osmsrbija.iz.rs/pretrazivaculica/?naselje="+mMaticniBrojNaselja+"&format=csv";
		System.out.println("link za pretragu: " + st);


		URL stockURL = null;
		try {
			stockURL = new URL(st);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog (null, "Greska u ucitavanju podataka sa servera", "Greska", JOptionPane.ERROR_MESSAGE);
		}

		int i = 0;
		Map<String, Integer> headers = new HashMap<>();

		try (BufferedReader br = new BufferedReader(new InputStreamReader(stockURL.openStream(), "UTF-8"))) {
			while ((line = br.readLine()) != null) {
				//sakuplja nazive kolona u prvom redu
				if (i == 0) {
					String[] headLine = line.split(",");
					for (int j = 0; j < headLine.length; j++) {
						headers.put(headLine[j].substring(1, headLine[j].length() - 1),
								j);
					}
					i++;
					continue;
				}
				//System.out.println(" - * - "+headers);

				String[] split = line.split(",");

				// TODO izgleda da je promenjen csv fajl koji daje Pedjin server !!!!
				if(split.length != 11) { // ima ulica sa zarezom u nazivu - one se sada ignorisu
					System.out.println("Ignorise zapis zbog zareza u imenu: " + line);
					continue;
				}

				mRGZ_MB_RGZaMap.put(split[headers.get("ulica_maticni_broj")].substring(1, split[headers.get("ulica_maticni_broj")].length() - 1), 
						split[headers.get("ulica_ime")].substring(1, split[headers.get("ulica_ime")].length() - 1));
			}

		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog (null, "Greska u ucitavanju podataka sa servera", "Greska", JOptionPane.ERROR_MESSAGE);
		}
		
		if(mRGZ_MB_RGZaMap.isEmpty()) {
			JOptionPane.showMessageDialog (null, "Nije ucitana ni jedna ulica !!!", "Greska", JOptionPane.ERROR_MESSAGE);
		}
	}

	public Map<String, String> getRGZ_MB_RGZaMap() {
		return mRGZ_MB_RGZaMap;
	}


	public String nadjiMBulicu(String nazivUlice) {
		String izlaz = "";

		if(!nazivUlice.isEmpty()) {
			for (Map.Entry<String,String> entry : mRGZ_MB_RGZaMap.entrySet()){
				System.out.println("Key = " + entry.getKey() +	", Value = " + entry.getValue()+ " - " +nazivUlice);
				if(nazivUlice.toUpperCase().equals(entry.getValue())) {
					System.out.println("     !!!!!!!!!!!!!!!  poklapanje sa  nazivom ulice ");
					String mbBrojUlice =  entry.getKey();
					if(izlaz.length()<2) {
						izlaz = mbBrojUlice;
					}
					else {
						izlaz = "!!!! " + mbBrojUlice + ", " + izlaz;		        		  
					}
				}
			}

		}
		return izlaz;
	}

	public String napraviUpitZaUlicu(String nazivUlice, boolean daLiUzimaNaseljeUobzir) {
		String izlaz = "";
		String line;
		String linkString = "https://www.osmsrbija.iz.rs/pretrazivaculica/api/?ulica=";

		URL stockURL = null;
		try {
			linkString = linkString + URLEncoder.encode(nazivUlice, "UTF-8");
			if(daLiUzimaNaseljeUobzir) {
				if(!mMaticniBrojNaselja.equals("?")) {
					linkString = linkString + "&naselje="+mMaticniBrojNaselja;				
				}
			}
			linkString = linkString +"&format=csv";
			System.out.println(linkString);
			stockURL = new URL(linkString);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog (null, "Greska u ucitavanju podataka sa servera", "Greska", JOptionPane.ERROR_MESSAGE);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i = 0;
		Map<String, Integer> headers = new HashMap<>();

		try (BufferedReader br = new BufferedReader(new InputStreamReader(stockURL.openStream(), "UTF-8"))) {
			while ((line = br.readLine()) != null) {
				//sakuplja nazive kolona u prvom redu
				if (i == 0) {
					String[] headLine = line.split(",");
					for (int j = 0; j < headLine.length; j++) {
						headers.put(headLine[j].replace("\"", ""), j);
					}
					i++;
					continue;
				}

				System.out.println(" - "+headers);

				String[] split = line.split(",");

				if(split.length != 11) { // ima ulica sa zarezom u nazivu - one se sada ignorisu
					System.out.println("Ignorise zapis zbog zareza u imenu: " + line);
					continue;
				}

				System.out.println(split[headers.get("ulica_maticni_broj")].substring(1, split[headers.get("ulica_maticni_broj")].length() - 1) 
						+ " - " + split[headers.get("ulica_ime")].substring(1, split[headers.get("ulica_ime")].length() - 1) 
						+ " - " + split[headers.get("tezina")]);
				izlaz += split[headers.get("ulica_maticni_broj")].substring(1, split[headers.get("ulica_maticni_broj")].length() - 1) + 
						" - " + split[headers.get("ulica_ime")].substring(1, split[headers.get("ulica_ime")].length() - 1) + 
						" - " + split[headers.get("tezina")] + 
						" - " + split[headers.get("naselje_maticni_broj")] + 
						" - " + split[headers.get("naselje_ime")] + 
						" - " + split[headers.get("opstina_maticni_broj")] + 
						" - " + split[headers.get("opstina_ime")] + "\n";
			}

		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog (null, "Greska u ucitavanju podataka sa servera", "Greska", JOptionPane.ERROR_MESSAGE);
		}
		return izlaz;
	}


	public void izbaciSaSpiskaMBUlice(String idUlice) {
		mRGZ_MB_RGZaMap.remove(idUlice);
	}


	public String getMaticniBrojNaselja() {
		return mMaticniBrojNaselja;
	}
}
