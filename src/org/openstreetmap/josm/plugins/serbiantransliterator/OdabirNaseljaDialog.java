package org.openstreetmap.josm.plugins.serbiantransliterator;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class OdabirNaseljaDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JList<String> list;
	private DefaultListModel<String> listModel;
	private JTextField filterField;
	private String result = "";
	private Preslovljavanje preslovljavanje;
	private List<String> listaNaselja;

	
	static int pozicijaProzora_x = 100;
	static int pozicijaProzora_y = 100;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			OdabirNaseljaDialog dialog = new OdabirNaseljaDialog(null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param listaNaselja2 
	 */
	public OdabirNaseljaDialog(List<String> pListaNaselja) {
		setTitle("Одабир насеља");
		setModal(true);
		setBounds(pozicijaProzora_x, pozicijaProzora_y, 390, 550);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		listModel = new DefaultListModel<String>();
		preslovljavanje = new Preslovljavanje();

		listaNaselja = pListaNaselja;
		
		popuniListuZaPrikaz("");

		//Create the list and put it in a scroll pane.
		list = new JList<String>(listModel);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {  
				JList<?> list = (JList<?>)evt.getSource();
				if (evt.getClickCount() == 2) { // za dupli klik
					int index = list.locationToIndex(evt.getPoint());
					result = listModel.get(index);
					System.out.println("dupli klik na izabrano: " + listModel.get(index));
					pozicijaProzora_x = getBounds().x;
					pozicijaProzora_y = getBounds().y;
					setVisible(false);
					dispose();
				}
			}
		});
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(1);
		list.setVisibleRowCount(23);

		JScrollPane listScrollPane = new JScrollPane(list);
		//listModel.getElementAt(list.getSelectedIndex()).toString();

		filterField = new JTextField();
		filterField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				popuniListuZaPrikaz(filterField.getText());
			}
		});

		// Listen for changes in the text
		filterField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				warn();
			}
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				popuniListuZaPrikaz(filterField.getText());
				//System.out.println(filterField.getText());
			}
		});

		filterField.setColumns(10);
		contentPanel.add(filterField, BorderLayout.NORTH);


		contentPanel.add(listScrollPane, BorderLayout.CENTER);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						result = (String) list.getSelectedValue();
						System.out.println("konacan odabir: "+result);
						pozicijaProzora_x = getBounds().x;
						pozicijaProzora_y = getBounds().y;
						setVisible(false);
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});

			}
		}
		
		// zatvara na ESC
		ActionListener escListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				OdabirNaseljaDialog.this.dispose();		        
				}
		};
		OdabirNaseljaDialog.this.getRootPane().registerKeyboardAction(escListener,
				KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				JComponent.WHEN_IN_FOCUSED_WINDOW);
	}


	String showDialog() {
		setVisible(true);
		return result;
	}

	private void popuniListuZaPrikaz(String filter) {
		filter = preslovljavanje.lat2cir(filter.toLowerCase());
		listModel.clear();

		for (String s : listaNaselja) {
//			System.out.println(filter + " " +s);
			if(s.toLowerCase().contains(filter)){
				listModel.addElement(s);
			}
		}
	}

}
